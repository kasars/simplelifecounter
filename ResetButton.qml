/*
 * SPDX-FileCopyrightText: 2019-2023 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick

Rectangle {
    id: resetBtn
    signal applyClicked(int value);

    property alias value: textInput.text
    color: "#20FFFFFF"

    property double intMargins: resetBtn.height * 0.05

    Column {
        id: columnItem
        anchors.fill: parent
        anchors.margins: resetBtn.intMargins
        spacing: resetBtn.intMargins

        TextInput {
            id: textInput
            anchors.left: parent.left
            anchors.right: parent.right
            height: columnItem.height * 0.45
            font.pixelSize: height * 0.9
            color: "white"
            text: value
            horizontalAlignment: Text.AlignHCenter
            validator: IntValidator {bottom: 0; top: 100;}
        }

        Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right
            height: resetBtn.intMargins * 0.5
            color: "#000000"
        }

        Image {
            height: columnItem.height * 0.4
            anchors.horizontalCenter: parent.horizontalCenter
            fillMode: Image.PreserveAspectFit
            source: "apply.png"
            MouseArea {
                anchors.fill: parent
                onClicked: applyClicked(value)
            }
        }
    }
}

