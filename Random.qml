/*
 * SPDX-FileCopyrightText: 2019-2023 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick

Item {
    id: randomItem
    signal closeClicked()

    property string defState: "Closed"
    function setOpen() { console.log("setOpen"); zoomItem.state = "Open"; }
    function setClosed() { zoomItem.state = "Closed"; }

    MouseArea {
        anchors.fill: parent
        onClicked: randomItem.closeClicked()
        visible: zoomItem.visible
    }

    ZoomItem {
        id: zoomItem
        state: defState
        anchors {
            left: parent.left
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
        targetHeight: randomItem.height

        Rectangle {
            anchors.fill: parent
            color: "#55000000"

            Dice {
                id: diceOne
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    bottom: parent.verticalCenter
                    margins: parent.width * 0.1
                }
                width: height
                opacity: randomizeTmr.running ? 0.7 : 1.0
            }
            Dice {
                id: diceTwo
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: parent.verticalCenter
                    bottom: parent.bottom
                    margins: parent.width * 0.1
                }
                width: height
                opacity: randomizeTmr.running ? 0.7 : 1.0
            }
        }

        onStateChanged: {
            console.log("onStateChanged:", state)
            if (state === "Open") {
                randomizeTmr.randCount = 15;
            }
        }
        Timer {
            id: randomizeTmr
            property int randCount: 0
            interval: 80
            running: randCount > 0
            onTriggered: {
                diceOne.value = Math.floor(Math.random() * Math.floor(6)) + 1;
                diceTwo.value = Math.floor(Math.random() * Math.floor(6)) + 1;
                randCount--;
            }
        }
    }
}

