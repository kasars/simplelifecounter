**Privacy Policy**

The Lite Counter app is an Open Source application. The application itself does not collect any information about the user. Settings are stored only on the device.

What usage statistics Google may collect, is their responsibility.
