/*
 * SPDX-FileCopyrightText: 2019-2023 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick

Item {
    id: zoomRect
    property double targetHeight: 10

    states: [
        State {
            name: "Open"
            PropertyChanges {
                target: zoomRect
                height: zoomRect.targetHeight

            }
        },
        State {
            name: "Closed"
            PropertyChanges { target: zoomRect; height: 0; }
        }
    ]

    transitions: [
      Transition {
          from: "Closed"; to: "Open"
          SequentialAnimation {
              ScriptAction { script: { zoomRect.visible = true; } }
              PropertyAnimation { target: zoomRect; properties: "height"; duration: 150 }
          }
      },
      Transition {
            from: "Open"; to: "Closed"
            SequentialAnimation {
                PropertyAnimation { target: zoomRect; properties: "height"; duration: 150 }
                ScriptAction { script: { zoomRect.visible = false; } }
            }
      }
    ]

    Component.onCompleted: {
        visible = state === "Open";
    }
}
