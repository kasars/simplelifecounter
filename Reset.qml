/*
 * SPDX-FileCopyrightText: 2019-2023 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtQuick.Layouts
import QtCore

Item {
    id: resetItem
    signal closeClicked()
    signal resetTo(int value)

    property string defState: "Closed"

    function setOpen() { zoomItem.state = "Open"; }
    function setClosed() { zoomItem.state = "Closed"; }

    MouseArea {
        anchors.fill: parent
        onClicked: resetItem.closeClicked()
        visible: zoomItem.visible
    }

    ZoomItem {
        id: zoomItem
        state: defState
        anchors {
            left: parent.left
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
        targetHeight: Math.min(resetItem.height * 0.3, resetItem.width)

        Rectangle {
            anchors.fill: parent
            color: "black"

            Row {
                id: rowItem
                anchors.fill: parent
                anchors.margins: parent.height * 0.1
                spacing: parent.height * 0.1

                ResetButton {
                    id: button1
                    height: parent.height
                    width: (parent.width - rowItem.spacing) / 2
                    value: "20"
                    onApplyClicked: resetItem.resetTo(value)
                }
                ResetButton {
                    id: button2
                    height: parent.height
                    width: (parent.width - rowItem.spacing) / 2
                    value: "30"
                    onApplyClicked: resetItem.resetTo(value)
                }
            }
        }
    }

    Settings {
         property alias resetValue1: button1.value
         property alias resetValue2: button2.value
     }
}

