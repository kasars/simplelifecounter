#!/bin/sh

APP_PID=$(adb shell pidof org.sars.thelitecounter)

adb logcat --pid $APP_PID
