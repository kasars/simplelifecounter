/*
 * SPDX-FileCopyrightText: 2019-2023 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick

Rectangle {
    property int value: 5
    radius: width * 0.07

    color: "white"

    Rectangle {
        id: topLeft
        anchors {
            top: parent.top
            left: parent.left
            margins: parent.width * 0.1
        }
        width: parent.width * 0.2
        height: width
        radius: width*0.5
        color: "black"
        visible: value === 4 || value === 5 || value === 6
    }

    Rectangle {
        id: topRight
        anchors {
            top: parent.top
            right: parent.right
            margins: parent.width * 0.1
        }
        width: parent.width * 0.2
        height: width
        radius: width*0.5
        color: "black"
        visible: value === 3 || value === 4 || value === 5 || value === 6
    }

    Rectangle {
        id: centerLeft
        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            margins: parent.width * 0.1
        }
        width: parent.width * 0.2
        height: width
        radius: width*0.5
        color: "black"
        visible: value === 2 || value === 6
    }

    Rectangle {
        id: center
        anchors.centerIn: parent
        width: parent.width * 0.2
        height: width
        radius: width*0.5
        color: "black"
        visible: value === 1 || value === 3 || value === 5
    }

    Rectangle {
        id: centerRight
        anchors {
            verticalCenter: parent.verticalCenter
            right: parent.right
            margins: parent.width * 0.1
        }
        width: parent.width * 0.2
        height: width
        radius: width*0.5
        color: "black"
        visible: value === 2 || value === 6
    }

    Rectangle {
        id: bottomLeft
        anchors {
            bottom: parent.bottom
            left: parent.left
            margins: parent.width * 0.1
        }
        width: parent.width * 0.2
        height: width
        radius: width*0.5
        color: "black"
        visible: value === 3 || value === 4 || value === 5 || value === 6
    }
    Rectangle {
        id: bottomRight
        anchors {
            bottom: parent.bottom
            right: parent.right
            margins: parent.width * 0.1
        }
        width: parent.width * 0.2
        height: width
        radius: width*0.5
        color: "black"
        visible: value === 4 || value === 5 || value === 6
    }
}
