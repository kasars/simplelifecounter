/*
 * SPDX-FileCopyrightText: 2019-2023 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtQuick.Layouts

RowLayout {
    id: menuItem
    signal resetPressed()
    signal randomPressed()

    Item {
        Layout.fillWidth: true
        Layout.fillHeight: true
    }

    Item {
        Layout.fillWidth: true
        Layout.fillHeight: true
        Image {
            width: menuItem.height
            height: menuItem.height
            source: "reload.png"
            MouseArea {
                anchors {
                    fill: parent
                    margins: -menuItem.height
                }
                onClicked: menuItem.resetPressed()
            }
        }
    }
    Item {
        Layout.fillWidth: true
        Layout.fillHeight: true
    }
    Item {
        Layout.fillWidth: true
        Layout.fillHeight: true
        Image {
            width: menuItem.height
            height: menuItem.height
            source: "random.png"
            MouseArea {
                anchors {
                    fill: parent
                    margins: -menuItem.height
                }
                onClicked: menuItem.randomPressed()
            }
        }
    }
    Item {
        Layout.fillWidth: true
        Layout.fillHeight: true
    }

}

