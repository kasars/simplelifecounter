/*
 * SPDX-FileCopyrightText: 2019-2023 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick 2.3
import QtSensors 5.11
import QtQuick.Window 2.6

Window {
    visible: true
    width: 480
    height: 800
    title: qsTr("The Lite Counter")

    Counter {
        id: counterOne
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: parent.verticalCenter
        }
        numberRotation: accel.orientation === portrait ? 0 :
                        accel.orientation === reversePortrait ? 180 :
                        accel.orientation === landscape ? 90 :
                        accel.orientation === reverseLandscape ? 270 : 180
    }
    Counter {
        id: counterTwo
        anchors {
            top: parent.verticalCenter
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        color: "red"

        numberRotation: accel.orientation === portrait ? 0 :
                        accel.orientation === reversePortrait ? 180 :
                        accel.orientation === landscape ? 90 :
                        accel.orientation === reverseLandscape ? 270 : 0
    }

    readonly property int onBack: 0
    readonly property int portrait: 1
    readonly property int landscape: 2
    readonly property int reverseLandscape: 3
    readonly property int reversePortrait: 4

    Accelerometer {
        id: accel
        dataRate: 100
        active: true

        property int orientation: onBack
        property bool accelAvailable: true


        onReadingChanged: {
            if (!accelAvailable) return;
            const x = accel.reading.x;
            const y = accel.reading.y;
            const z = accel.reading.z;
            const limit = 9;
            if (Math.abs(z) > limit) orientation = onBack;
            else if (x > limit)      orientation = landscape;
            else if (x < -limit)     orientation = reverseLandscape;
            else if (y > limit)      orientation = portrait;
            else if (y < -limit)     orientation = reversePortrait;
        }
    }

    Component.onCompleted: {
        var types = QmlSensors.sensorTypes();
        if (types.indexOf("QAccelerometer") === -1) {
            accel.accelAvailable = false;
        }
    }

    FocusScope {
        anchors.fill: parent
        focus: true

        Menu {
            id: menu
            anchors {
                left: parent.left
                right: parent.right
                verticalCenter: parent.verticalCenter
            }
            height: parent.height * 0.1

            onResetPressed: {
                resetPage.setOpen();
            }
            onRandomPressed: {
                randomPage.setOpen();
            }
        }

        Random {
            id: randomPage
            anchors.fill: parent
            onCloseClicked: { setClosed(); }
        }

        Reset {
            id: resetPage
            anchors.fill: parent
            onCloseClicked: { setClosed(); }
            onResetTo: {
                counterOne.value = value;
                counterTwo.value = value;
                setClosed();
            }
        }

        Keys.onPressed: {
            event.accepted = true;
        }
        Keys.onReleased: {
            if (event.key === Qt.Key_Back || event.key === Qt.Key_Escape) {
                randomPage.setClosed();
                resetPage.setClosed();
            }
            event.accepted = true;
        }
    }
}
