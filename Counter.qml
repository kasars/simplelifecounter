/*
 * SPDX-FileCopyrightText: 2019-2023 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick

Rectangle {
    id: counter
    property int value: 0
    color: "green"
    property string textColor: "white"

    property alias numberRotation: valueLabel.rotation

    Text {
        id: valueLabel
        anchors.centerIn: parent
        font.pixelSize: Math.min(parent.width, parent.height) * 0.4
        text: value
        font.underline: value === 6 || value === 9 || value === 60 || value === 90 || value === 66 || value === 99
        color: textColor
        Behavior on rotation { NumberAnimation { duration: 200 } }
    }
    Rectangle {
        id: adder
        anchors {
            verticalCenter: parent.verticalCenter
            right: parent.right
            rightMargin: parent.width * 0.08
        }

        width: Math.min(parent.width, parent.height) * 0.2
        height: width

        radius: width * 0.15

        border.color: "white"
        border.width: width * 0.03
        color: "#00000000"

        Text {
            anchors.centerIn: parent
            font.pixelSize: parent.height * 0.75
            text: "+"
            color: textColor
        }

        MouseArea {
            anchors {
                fill: parent
                margins: -parent.width
            }
            onClicked: {
                counter.value++;
            }
        }
    }

    Rectangle {
        id: subber
        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            leftMargin: parent.width * 0.08
        }

        width: Math.min(parent.width, parent.height) * 0.2
        height: width

        radius: width * 0.15

        border.color: "white"
        border.width: width * 0.03
        color: "#00000000"

        Text {
            anchors.centerIn: parent
            font.pixelSize: parent.height * 0.75
            text: "-"
            color: textColor
        }

        MouseArea {
            anchors {
                fill: parent
                margins: -parent.width
            }
            onClicked: {
                counter.value--;
            }
        }
    }
}
